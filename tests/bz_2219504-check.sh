#!/bin/bash

OUTPUT=`rpm -qR perl-Date-Manip`

if ! echo "$OUTPUT" | grep -q "MODULE_COMPAT_"; then
    exit 0
else
    exit 1
fi
